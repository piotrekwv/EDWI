# -*- coding: utf-8 -*-
import urllib2
import re
import string
import locale
import numpy as np
 
wyrazy=[]
locale.setlocale(locale.LC_ALL, 'C')
zoo=['https://pl.wikipedia.org/wiki/Afrorudzik_oliwkowy','https://pl.wikipedia.org/wiki/Afrorudzik_oliwkowy','https://pl.wikipedia.org/wiki/Krawczyk_jasnolicy','https://pl.wikipedia.org/wiki/Kr%C4%99tolotek_rdzawy','https://pl.wikipedia.org/wiki/Krytonosek_kolumbijski','https://pl.wikipedia.org/wiki/Problem_obliczeniowy']
allwordlist=[]
words=[]
clist=[]
def nice(adres):
    response = urllib2.urlopen(adres)
    html = response.read()
    html=html.decode('utf-8','ignore')
    nowyhtml=re.sub('<style[^<>]*?>.*?</style>(?s)', '',html)
    nowyhtml=re.sub('<script[^<>]*?>.*?</script>(?s)', '',nowyhtml)
    nowyhtml=re.sub('<.*?>(?s)', '',nowyhtml)
    nowyhtml = re.sub('<!--[\s\S]*?-->','',nowyhtml)
    nowyhtml=re.sub('&[^<>]*?;', '',nowyhtml)
    nowyhtml=re.sub('\s+', ' ',nowyhtml)
    nowyhtml=nowyhtml.lower()
    for punct in string.punctuation:
        nowyhtml = nowyhtml.replace(punct," ")
    allwordlist=nowyhtml.split(); 
    return allwordlist
 
for adres in zoo:
    out=sorted(nice(adres))
    counter=0
    for i in out:
        counter=counter+1
        wyrazy.append(i);
    print "Liczba slow w " + adres+ " to " + str(counter)
    clist.append(counter)
print u"wszystkich wyraz�w jest " + str(len(wyrazy))
print u"wszystkich RӯNYCH wyraz�w jest " + str(len(set(wyrazy)))
wyrazy=sorted(set(wyrazy))
wyrazy.sort()
#print u"S�ownik tworz� wyrazy: "
#for wyraz in wyrazy:
 #   print wyraz
 
 
vector=[]
vectorlist=[]
it=0
for adres in zoo:
    out=sorted(nice(adres))
    for word in wyrazy:
        occurance=0
        for i in out:
            if i==word:
                occurance=occurance+1
        vector.append(float(occurance)/float(clist[it]))
    it=it+1  
    vectorlist.append(vector)
    vector=[]
#for vectors in vectorlist:
#    print vectors
 
npary=[] 
for x in range(len(vectorlist)):
    for y in range (x+1,len(vectorlist)):
        index= str(x+1) + " " + str(y+1)
        A=np.asarray(vectorlist[x])
        B=np.asarray(vectorlist[y])
        M1=np.linalg.norm(A)
        M2=np.linalg.norm(B)
        npary.append ([index,np.dot(A, B)/np.dot(M1,M2)])
cos=sorted(npary, key=lambda npary: npary[1]) 
for element in cos:
    print element