/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package twitteranalyzer;

import java.util.ArrayList;

/**
 *
 * @author Piotrek
 */
public class WhatToThink {
    public static void main(String[] args) {
        String topic = "winning";
        ArrayList<String> tweets = TweetManager.getTweets(topic);
        NLP.init();
        for(String tweet : tweets) {
            System.out.println(tweet + " : " + NLP.findSentiment(tweet));
        }
    }
}
