import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
 
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
 
public class Lucene5ex {
    public static void main(String[] args) throws IOException, ParseException {
        // 0. Specify the analyzer for tokenizing text.
        //    The same analyzer should be used for indexing and searching
        StandardAnalyzer analyzer = new StandardAnalyzer();
 
        // 1. create the index
        Directory index = new RAMDirectory();
 
        IndexWriterConfig config = new IndexWriterConfig(analyzer);
 
        IndexWriter w = new IndexWriter(index, config);
        File directory = new File("D:\\pliki");
                    File files[] = directory.listFiles();
                    for (File f : files) {
                        Scanner scanner = new Scanner(f);
                        String tytul;
                        BufferedReader inputStream = new BufferedReader(new FileReader(f));
                        String line;
                        while ((line = inputStream.readLine()) != null) {
                            if (line.matches("Title:.*$")) {
                                line=line.substring(7);
                                System.out.println(line);
                                addDoc(w, line);
                            }
                        }
                    }
          w.close();
        // 2. query
          
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
String s = bufferRead.readLine();
        String querystr = s.trim();
 
        // the "title" arg specifies the default field to use
        // when no field is explicitly specified in the query.
        Query q = new QueryParser("title", analyzer).parse(querystr);
 
        // 3. search
        int hitsPerPage = 10;
        IndexReader reader = DirectoryReader.open(index);
        IndexSearcher searcher = new IndexSearcher(reader);
        TopDocs docs = searcher.search(q, hitsPerPage);
        ScoreDoc[] hits = docs.scoreDocs;
 
        // 4. display results
        System.out.println("Found " + hits.length + " hits.");
        for(int i=0;i<hits.length;++i) {
            int docId = hits[i].doc;
            Document d = searcher.doc(docId);
            System.out.println((i + 1) + "\t" + d.get("title"));
        }
 
        // reader can only be closed when there
        // is no need to access the documents any more.
        reader.close();
    }
 
    private static void addDoc(IndexWriter w, String title) throws IOException {
        Document doc = new Document();
        doc.add(new TextField("title", title, Field.Store.YES));
 
        // use a string field for isbn because we don't want it tokenized
  
        w.addDocument(doc);
    }
}


 
