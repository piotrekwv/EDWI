package javaapplication4;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

public class Lucene6ex {

    public static void main(String[] args) throws IOException, ParseException {

        

        StandardAnalyzer analyzer = new StandardAnalyzer();

        boolean recreateIndexIfExists = true;
        Path plik = Paths.get("C:\\Users\\student\\Desktop\\pliki");
        Directory index = FSDirectory.open(plik);

        IndexWriterConfig config = new IndexWriterConfig(analyzer);
        
        boolean indexexist = true;
        if (!indexexist) {

            if (recreateIndexIfExists) {
                config.setOpenMode(OpenMode.CREATE);
            } else {
            
                config.setOpenMode(OpenMode.CREATE_OR_APPEND);
            }

            IndexWriter w = new IndexWriter(index, config);

            File directory = new File("C:\\Users\\student\\Desktop\\pliki");

            File files[] = directory.listFiles();

            for (File f : files) {

                String tytul = "";
                String body = "";

                BufferedReader inputStream = new BufferedReader(new FileReader(f));

                String line;
                int lineno = 0;

                while ((line = inputStream.readLine()) != null) {

                    lineno++;
                    if (line.matches("Title:.*$")) {

                        tytul = line.substring(7);

                        System.out.println(line);

                    } else {
                        if (lineno > 20) {

                            body = body + line;

                        }
                    }

                }

                if (tytul != null && body != null)
                    addDoc(w, tytul, body);
                inputStream.close();
            }

            w.close();

            
        }
        boolean control=true;
        ////INTERFEJSxd
        while (control){
        System.out.println("Chcesz szukać w a-tytulach, b-tekscie?");
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        String s = bufferRead.readLine();
        switch(s){
        case "a":
            System.out.println("Podaj slowo");
            BufferedReader b1 = new BufferedReader(new InputStreamReader(System.in));
            String title = b1.readLine();
            String querystr = title.trim()+"*";
            Query q = new QueryParser("title", analyzer).parse(querystr);
            display(q,index);
            break;
        
        case "b":
            System.out.println("Podaj slowo");
            BufferedReader b2 = new BufferedReader(new InputStreamReader(System.in));
            String slowo = b2.readLine();
            String querystr2 = "\""+slowo+"\"";
            Query m = new QueryParser("body", analyzer).parse(querystr2);
            display(m,index);
            break;
        case "k":
            control=false;
        default:
            break;
        }
        
    
        }
        
        
        
        
        
        
    }

    static void display(Query q, Directory index) throws IOException{
        int hitsPerPage = 10;

        IndexReader reader = DirectoryReader.open(index);

        IndexSearcher searcher = new IndexSearcher(reader);

        TopDocs docs = searcher.search(q, hitsPerPage);

        ScoreDoc[] hits = docs.scoreDocs;



        System.out.println("Found " + hits.length + " hits.");

        for (int i = 0; i < hits.length; ++i) {

            int docId = hits[i].doc;

            Document d = searcher.doc(docId);

            System.out.println((i + 1) + "\t" + d.get("title"));

        }

    
        reader.close();

        }
    
    
    
    private static void addDoc(IndexWriter w, String title, String body) throws IOException {

        Document doc = new Document();

        doc.add(new TextField("title", title, Field.Store.YES));
        doc.add(new TextField("body", body, Field.Store.YES));


        w.addDocument(doc);

    }
    

}