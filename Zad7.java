package webcrawler;
 
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
 
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
 
 
public class Zad7 {
 
    static IndexWriter w;
    static int deep=0;
    public static void main(String[] args) throws IOException, ParseException, IllegalArgumentException {
 
 
 
 
        StandardAnalyzer analyzer = new StandardAnalyzer();
 
        boolean recreateIndexIfExists = false;
        Path plik = Paths.get("C:\\Users\\student\\Desktop\\test");
        Directory index = FSDirectory.open(plik);
        ArrayList toprocess=new ArrayList();
        ArrayList toprocess2=new ArrayList();
        
        IndexWriterConfig config = new IndexWriterConfig(analyzer);
       
        boolean indexexist = false;
        if (!indexexist) {
 
            if (recreateIndexIfExists) {
                config.setOpenMode(OpenMode.CREATE);
            } else {
                // Add new documents to an existing index:
                config.setOpenMode(OpenMode.CREATE_OR_APPEND);
            }
 
            w = new IndexWriter(index, config);
 
            createRobotList(new URL("http://elektroda.pl"));
            toprocess=processOnePage("http://elektroda.pl");
 
           // wykop.pl
           ///forumpc.pl
               
                   //        elektroda.pl
            
            while(deep!=0){
                deep--;
                System.out.println("Kolejna Iteracja");
                if(!toprocess2.isEmpty())
                {
                     for(int i=0;i<toprocess2.size()-1;i++)
                    {    
                    toprocess=processOnePage((String) toprocess2.get(i));
                    } 
                    toprocess2.clear();
                }   
                
                if(!toprocess.isEmpty())
                {                
                    for(int i=0;i<toprocess.size()-1;i++)
                    {    
                        toprocess2=processOnePage((String) toprocess.get(i));
                    }    
                    toprocess.clear();
                }
            }
            
            
                
            
 
            w.close();
        }
        boolean control=true;
        while (control){
        System.out.println("Chcesz szukać w a-tytulach, b-tekscie?");
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        String s = bufferRead.readLine();
        switch(s){
        case "a":
            System.out.println("Podaj slowo");
            BufferedReader b1 = new BufferedReader(new InputStreamReader(System.in));
            String title = b1.readLine();
            String querystr = title.trim()+"*";
            Query q = new QueryParser("title", analyzer).parse(querystr);
            display(q,index);
            break;
        
        case "b":
            System.out.println("Podaj slowo");
            BufferedReader b2 = new BufferedReader(new InputStreamReader(System.in));
            String slowo = b2.readLine();
            String querystr2 = "\""+slowo.trim()+"\"";
            Query m = new QueryParser("body", analyzer).parse(querystr2);
            display(m,index);
            break;
        case "k":
            control=false;
        default:
            break;
        }
        
    
        }
        
        
        
        
      
        
    }
 
    static void display(Query q, Directory index) throws IOException{
        int hitsPerPage = 5;
 
        IndexReader reader = DirectoryReader.open(index);
 
        IndexSearcher searcher = new IndexSearcher(reader);
 
        TopDocs docs = searcher.search(q, hitsPerPage);
 
        ScoreDoc[] hits = docs.scoreDocs;
 
        System.out.println("Found " + hits.length + " hits.");
 
        for (int i = 0; i < hits.length; ++i) {
 
            int docId = hits[i].doc;
 
            Document d = searcher.doc(docId);
 
            System.out.println((i + 1) + "\t" + d.get("title"));
 
        }
 
    
        reader.close();
 
        }
    
    
    
    private static void addDoc(IndexWriter w, String title, String body) throws IOException {
 
        Document doc = new Document();
 
        doc.add(new TextField("title", title, Field.Store.YES));
        doc.add(new TextField("body", body, Field.Store.YES));
 
 
        w.addDocument(doc);
 
    }
    
    ///////////////////////////////////////////////////////
    public static ArrayList processOnePage(String URL) throws IOException{
         
        String tytul = "";
        String body = "";
        ArrayList addresses = new ArrayList();
            org.jsoup.nodes.Document doc = Jsoup.connect(URL).get();
            Elements questions = doc.select("a[href]");
            for(Element link: questions){
                      
               addDoc(w, link.attr("abs:href"),Jsoup.connect(link.attr("abs:href")).ignoreContentType(true).followRedirects(false).ignoreHttpErrors(true).timeout(0).get().toString());
                System.out.println(link.attr("abs:href"));
                if(checkRobotList(link.attr("abs:href")))
            addresses.add(link.attr("abs:href"));
                 
            }
        return addresses;
    
    }
    private static ArrayList disallowList;
    
    
    private static void createRobotList(URL urlToCheck) throws MalformedURLException, IOException {
        String host = urlToCheck.getHost().toLowerCase();  
       System.out.println("ZAKAZANE ADRESY");
            disallowList = new ArrayList();
             
          
                URL robotsFileUrl =
                        new URL("http://" + host + "/robots.txt");
                 
                // Open connection to robot file URL for reading.
                BufferedReader reader =
                        new BufferedReader(new InputStreamReader(
                        robotsFileUrl.openStream()));
                 
                // Read robot file, creating list of disallowed paths.
                String line;
                while ((line = reader.readLine()) != null) {
                    if (line.indexOf("Disallow:") == 0) {
                        String disallowPath =
                                line.substring("Disallow:".length());
                         
                        // Check disallow path for comments and 
                        // remove if present.
                        int commentIndex = disallowPath.indexOf("#");
                        if (commentIndex != - 1) {
                            disallowPath =
                                    disallowPath.substring(0, commentIndex);
                        }
                         
                        // Remove leading or trailing spaces from 
                        // disallow path.
                        disallowPath = disallowPath.trim();
                         
                        // Add disallow path to list.
                        disallowList.add(disallowPath);
                          System.out.println(disallowPath);
                    }
                }
        System.out.println("KONIEC ZAKAZANYCH ADRESÓW");
    }
    
      private static boolean checkRobotList(String urlToCheck){
 
      
            for (int i = 0; i < disallowList.size(); i++) {
             String disallow = (String) disallowList.get(i);
                if (urlToCheck.startsWith(disallow)) {
                         return false;
                     }
            }
 
          
        return true;
            
        }
    
    
    
    
    
    
    
    
 
}
 